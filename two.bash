#!/bin/bash
sudo yum update -y
sudo yum install -y httpd
sudo systemctl start httpd
sudo systemctl enable httpd
sudo mkdir -p /var/www/html
sudo wget -O /var/www/html/index.html "https://procore-code-public.s3.amazonaws.com/index.html"
sudo systemctl restart httpd