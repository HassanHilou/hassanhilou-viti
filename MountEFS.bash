#!/bin/bash

# Mount EFS
sudo yum update -y
sudo yum install -y amazon-efs-utils
sudo mkdir /mnt/efs
sudo mount -t efs fs-08b7cf103284c460f:/ /mnt/efs

# Configure login banner
sudo bash -c 'cat >/etc/ssh/banner <<EOL
* * * * * * * W A R N I N G * * * * * * * * * *
This computer system is the property of ProCore Plus. It is for authorized use only. By using this system, all users acknowledge notice of, and agree to comply with, the Acceptable Use of Information Technology Resources Policy (“AUP”). Unauthorized or improper use of this system may result in administrative disciplinary action, civil charges/criminal penalties, and/or other sanctions as set forth in the AUP. By continuing to use this system you indicate your awareness of and consent to these terms and conditions of use. LOG OFF IMMEDIATELY if you do not agree to the conditions stated in this warning.
* * * * * * * * * * * * * * * * * * * *
EOL'
sudo bash -c 'echo "Banner /etc/ssh/banner" >> /etc/ssh/sshd_config'

# Restart SSH
sudo systemctl restart sshd


Lab-env-1:
#!/bin/bash
yum update -y
yum install -y wget ca-certificates
wget -O - https://packages.cisofy.com/keys/cisofy-software-public.key | sudo tee /etc/pki/rpm-gpg/RPM-GPG-KEY-CISOFY-$(uname -r) >/dev/null
rpm --import /etc/pki/rpm-gpg/RPM-GPG-KEY-CISOFY-$(uname -r)
echo -e "[lynis]\nname=CISOfy Software - Lynis package\nbaseurl=https://packages.cisofy.com/community/lynis/rpm/\$releasever/\$basearch/\ngpgcheck=1\ngpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-CISOFY-\$(uname -r)\nenabled=1" | sudo tee /etc/yum.repos.d/lynis.repo >/dev/null
yum makecache fast
yum install -y lynis

Was-cloud9-ticket-10:
#!/bin/bash

UNIX_USER="ec2-user"
UNIX_USER_HOME="/home/ec2-user"
ENVIRONMENT_PATH="/home/ec2-user/environment"
UNIX_GROUP=$(id -g -n "$UNIX_USER")

# Apply security patches
OPERATING_SYSTEM=$(awk -F= '$1=="ID" { print $2 ;}' /etc/os-release | sed -e 's/^"//' -e 's/"$//')
if [ "$OPERATING_SYSTEM" == "amzn" ]; then
    yum -q -y update --security > /tmp/init-yum-update-security 2>&1 &
elif [ "$OPERATING_SYSTEM" == "ubuntu" ]; then
    unattended-upgrade &
fi

# add SSH key
install -g "$UNIX_GROUP" -o "$UNIX_USER" -m 755 -d "$UNIX_USER_HOME"/.ssh
cat <<'EOF' >> "$UNIX_USER_HOME"/.ssh/authorized_keys
# Important
# ---------
# The following public key is required by Cloud9 IDE
# Removing this key will make this EC2 instance inaccessible by the IDE
#
cert-authority ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCf9IwXSF3wzcexDSyOtRoCBTxSChejLe2NiB9kAnavIwROrw1Y8ExgOCnHtS+Q+aGKuwJsNOzGOubRwk1udEJb8q2PnRBVhyqfjx0semYC3X3LqJXdvwNulr3OorWu8pSwUgXC4u5bAHWdxWEteXvjXJhNg1ZkdFatBQryv4uNQTqvtiSqcOonb/tr9RneYwaWQLs5Z6uzFXHhFitoBn/Afv/MIN6OHxU3oWRaQEeWHGERWhiijwEnIPgdsiN0BgfUxDot8QAA1rcGZN55UcNjpHBSvR9Gc+5s5YmKGkuW010BiYX2/zBY0JDCBcviiRlexFbLfL8q8+ijhHyoQ8QiFygHiu1lkW8VsjY66gxWLxYUIxrnbLtdKp982SjW3mYh4yDy8+xbmOGv6ENf04djP/Z+2rOXMZgQpsSAbxTHDjuYRdJDlgGpJTDeSx+M4Mzl/f5lLacINAdPU79oH82TKtrfYYJIyOQFXlj2IAtXMtH37nv0UaNVSwZziLu2d+Z8hnQ+zTZAKkYs6/b7xND6TRNeHGro5fBcjMkT38YvRTRj/m+0D2hnka3LklLuI+0AZsXBwS/b59lSTWb5YidZX3OTsXgNThSuDqL9T6nl0LAcoRC6kDKWrkJtUmK5pNwSMw4YPhPEp4+g3JaV0J/A/CnrBOQdY/Z7iVci5zJ66Q== 99820297c4f84c81a7b0981fc9f2d605@cloud9.amazon.com


#
# Add any additional keys below this line
#

EOF

# allow automatic shutdown
echo "$UNIX_USER    ALL=(ALL) NOPASSWD: /sbin/poweroff, /sbin/reboot, /sbin/shutdown" >> /etc/sudoers

ln -s /opt/c9 "$UNIX_USER_HOME"/.c9
chown -R "$UNIX_USER":"$UNIX_GROUP" "$UNIX_USER_HOME"/.c9 /opt/c9
install -g "$UNIX_GROUP" -o "$UNIX_USER" -m 755 -d "$ENVIRONMENT_PATH"

if [ "$ENVIRONMENT_PATH" == "/home/ec2-user/environment" ] && grep "alias python=python27" "$UNIX_USER_HOME"/.bashrc; then

    cat <<'EOF' > "$UNIX_USER_HOME"/.bashrc
# .bashrc

export PATH=$PATH:$HOME/.local/bin:$HOME/bin

# load nvm
export NVM_DIR="$HOME/.nvm"
[ "$BASH_VERSION" ] && npm() {
    # hack: avoid slow npm sanity check in nvm
    if [ "$*" == "config get prefix" ]; then which node | sed "s/bin\/node//";
    else $(which npm) "$@"; fi
}
# [ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh"  # This loads nvm
rvm_silence_path_mismatch_check_flag=1 # prevent rvm complaints that nvm is first in PATH
unset npm # end hack


# User specific aliases and functions
alias python=python27

# modifications needed only in interactive mode
if [ "$PS1" != "" ]; then
    # Set default editor for git
    git config --global core.editor nano

    # Turn on checkwinsize
    shopt -s checkwinsize

    # keep more history
    shopt -s histappend
    export HISTSIZE=100000
    export HISTFILESIZE=100000
    export PROMPT_COMMAND="history -a;"

    # Source for Git PS1 function
    if ! type -t __git_ps1 && [ -e "/usr/share/git-core/contrib/completion/git-prompt.sh" ]; then
        . /usr/share/git-core/contrib/completion/git-prompt.sh
    fi

    # Cloud9 default prompt
    _cloud9_prompt_user() {
        if [ "$C9_USER" = root ]; then
            echo "$USER"
        else
            echo "$C9_USER"
        fi
    }

    PS1='\[\033[01;32m\]$(_cloud9_prompt_user)\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]$(__git_ps1 " (%s)" 2>/dev/null) $ '
fi

EOF

    chown "$UNIX_USER":"$UNIX_GROUP" "$UNIX_USER_HOME"/.bashrc
fi

if [ "$ENVIRONMENT_PATH" == "/home/ec2-user/environment" ] && [ ! -f "$ENVIRONMENT_PATH"/README.md ]; then
    cat <<'EOF' >> "$ENVIRONMENT_PATH"/README.md
         ___        ______     ____ _                 _  ___
        / \ \      / / ___|   / ___| | ___  _   _  __| |/ _ \
       / _ \ \ /\ / /\___ \  | |   | |/ _ \| | | |/ _` | (_) |
      / ___ \ V  V /  ___) | | |___| | (_) | |_| | (_| |\__, |
     /_/   \_\_/\_/  |____/   \____|_|\___/ \__,_|\__,_|  /_/
 -----------------------------------------------------------------


Hi there! Welcome to AWS Cloud9!

To get started, create some files, play with the terminal,
or visit https://docs.aws.amazon.com/console/cloud9/ for our documentation.

Happy coding!

EOF

    chown "$UNIX_USER":"$UNIX_GROUP" "$UNIX_USER_HOME"/environment/README.md
fi

# Fix for permission error when trying to call `gem install`
chown "$UNIX_USER" -R /usr/local/rvm/gems

#This script is appended to another bash script, so it does not need a bash script file header.

UNIX_USER_HOME="/home/ec2-user"

C9_DIR=$UNIX_USER_HOME/.c9
CONFIG_FILE_PATH="$C9_DIR"/autoshutdown-configuration
VFS_CHECK_FILE_PATH="$C9_DIR"/stop-if-inactive.sh
CONFIG_METRIC_FILE_PATH="$C9_DIR"/autoshutdown-timestamp

echo "SHUTDOWN_TIMEOUT=30" > "$CONFIG_FILE_PATH"
chmod a+w "$CONFIG_FILE_PATH"

touch "$CONFIG_METRIC_FILE_PATH"
chmod a+wr "$CONFIG_METRIC_FILE_PATH"

echo -e '#!/bin/bash
set -euo pipefail
CONFIG=$(cat '$CONFIG_FILE_PATH')
SHUTDOWN_TIMEOUT=${CONFIG#*=}
if ! [[ $SHUTDOWN_TIMEOUT =~ ^[0-9]*$ ]]; then
    echo "shutdown timeout is invalid"
    exit 1
fi
is_shutting_down() {
    is_shutting_down_ubuntu &> /dev/null || is_shutting_down_al1 &> /dev/null || is_shutting_down_al2 &> /dev/null
}
is_shutting_down_ubuntu() {
    local TIMEOUT
    TIMEOUT=$(busctl get-property org.freedesktop.login1 /org/freedesktop/login1 org.freedesktop.login1.Manager ScheduledShutdown)
    if [ "$?" -ne "0" ]; then
        return 1
    fi
    if [ "$(echo $TIMEOUT | awk "{print \$3}")" == "0" ]; then
        return 1
    else
        return 0
    fi
}
is_shutting_down_al1() {
    pgrep shutdown
}
is_shutting_down_al2() {
    local FILE
    FILE=/run/systemd/shutdown/scheduled
    if [[ -f "$FILE" ]]; then
        return 0
    else
        return 1
    fi
}
is_vfs_connected() {
    pgrep -f vfs-worker >/dev/null
}

if is_shutting_down; then
    if [[ ! $SHUTDOWN_TIMEOUT =~ ^[0-9]+$ ]] || is_vfs_connected; then
        sudo shutdown -c
        echo > "'$CONFIG_METRIC_FILE_PATH'"
    else
        TIMESTAMP=$(date +%s)
        echo "$TIMESTAMP" > "'$CONFIG_METRIC_FILE_PATH'"
    fi
else
    if [[ $SHUTDOWN_TIMEOUT =~ ^[0-9]+$ ]] && ! is_vfs_connected; then
        sudo shutdown -h $SHUTDOWN_TIMEOUT
    fi
fi' > "$VFS_CHECK_FILE_PATH"

chmod +x "$VFS_CHECK_FILE_PATH"

echo "* * * * * root $VFS_CHECK_FILE_PATH" > /etc/cron.d/c9-automatic-shutdown
