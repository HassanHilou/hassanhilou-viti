variable "aws_region" {
  description = "AWS Region"
  default     = "us-east-1"
}

variable "instance_type" {
  description = "EC2 Instance Type"
  default     = "t2.micro"
}

variable "vpc_identifier" {
  description = "VPC ID"
  default     = "vpc-03fd9a922bfaa384b"
}

variable "subnet_identifiers" {
  description = "List of Subnet IDs"
  type        = list(string)
  default     = ["subnet-013c8b63a70b5158d", "subnet-0ede3dd0a75bebe95"]
}

variable "security_group_id" {
  description = "Security Group ID"
  default     = "sg-01d08a019f5fdc3d1"
}

variable "database_name" {
  description = "Database Name"
  default     = "db_new"
}

variable "database_username" {
  description = "Database Username"
  default     = "hassan"
}

variable "database_password" {
  description = "Database Password"
  default     = "abcd1234"
}

variable "database_storage" {
  description = "Database Storage (in GB)"
  default     = 5
}

variable "ssh_ingress_cidr_blocks" {
  description = "CIDR block for SSH access"
  default     = "0.0.0.0/0"
}
