terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = var.aws_region
}

data "aws_ami" "amazon_linux" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-*-x86_64-gp2"]
  }
}

resource "aws_instance" "wordpress_instance" {
  ami                    = data.aws_ami.amazon_linux.id
  instance_type          = var.instance_type
  key_name               = "new12"  # Replace with your key pair name
  vpc_security_group_ids = [var.security_group_id]
  iam_instance_profile   = "ticket21"  # Replace with your IAM instance profile
  subnet_id              = ["subnet-013c8b63a70b5158d", "subnet-0ede3dd0a75bebe95"]  # Replace with the correct subnet IDs
}

  user_data = data.template_file.user_data.rendered
}

resource "aws_security_group" "rds_security_group" {
  vpc_id = "vpc-03fd9a922bfaa384b"  # Specify the VPC ID
}

resource "aws_db_subnet_group" "rds_subnet_group" {
  name        = "my-rds-subnet-group"
  description = "My RDS subnet group"
  subnet_ids  = "subnet-013c8b63a70b5158d"  # Replace with the correct subnet ID from the desired VPC
}

resource "aws_db_instance" "wordpressdb" {
  identifier                = "db-new"  # Replace with the desired database identifier
  engine                    = "mysql"
  engine_version            = "5.7.38"  # Replace with the desired engine version
  instance_class            = "db.t2.micro"
  allocated_storage         = 5
  name                      = var.database_name
  username                  = var.database_username
  password                  = var.database_password
  publicly_accessible       = true
  

  vpc_security_group_ids    = [aws_security_group.rds_security_group.id]
  db_subnet_group_name      = aws_db_subnet_group.rds_subnet_group.name

  deletion_protection       = false
  skip_final_snapshot       = true
  multi_az                  = false
  backup_retention_period   = 7
}



data "template_file" "user_data" {
  template = file("userdata.sh")
  vars = {
    db_username      = "hassan"
    db_user_password = "abcd1234"
    db_name          = "db_new"
    db_RDS           = aws_db_instance.wordpressdb.endpoint
  }
}

resource "null_resource" "userdata_reference" {
  triggers = {
    user_data = data.template_file.user_data.rendered
  }
}

resource "aws_security_group_rule" "ec2_to_rds" {
  security_group_id        = tolist(aws_db_instance.wordpressdb.vpc_security_group_ids)[0]
  source_security_group_id = tolist(aws_instance.wordpress_instance.vpc_security_group_ids)[0]
  type                     = "ingress"
  from_port                = 3306
  to_port                  = 3306
  protocol                 = "tcp"
}

output "ec2_instance_public_ip" {
  value       = aws_instance.wordpress_instance.public_ip
  description = "Public IP address of the EC2 instance"
}
